import { NavigationContainer } from "@react-navigation/native";
import React from "react";
import MainStack from "./MainStack";

// on créée un composant qui va initialiser notre routing
// pour que la navigation marche il faut:
// 1. englober toute l'app avec NavigationContainer
// 2. mettre une stack en enfant de NavigationContainer (cette stack va contenir des écrans)
const MainNavigator = () => {
  return (
    <NavigationContainer>
      <MainStack />
    </NavigationContainer>
  );
};

export default MainNavigator;
