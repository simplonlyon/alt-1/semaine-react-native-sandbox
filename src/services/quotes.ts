import axios from "axios";

export const getRandomQuote = async () => {
  //    return axios
  //     .get<IQuote>("https://api.whatdoestrumpthink.com/api/v1/quotes/random")
  //     .then((response) => response.data);

  const response = await axios.get<IQuote>(
    "https://api.whatdoestrumpthink.com/api/v1/quotes/random"
  );

  return response.data;
};
