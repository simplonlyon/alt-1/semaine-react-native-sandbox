import { Feather } from "@expo/vector-icons";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  useWindowDimensions,
  View,
} from "react-native";
import IconButton from "../shared/IconButton";

type Props = {
  onAdd: (taskTitle: string) => void;
  label: string;
};

const AddTaskForm = ({ onAdd, label }: Props) => {
  const [value, setValue] = useState("");
  const { width } = useWindowDimensions(); // permet d'avoir les dimensions, ici la width, de notre device (utile pour le responsive)

  const handleAddTask = (value: string) => {
    // si je n'ai pas de texte saisi dans mon input alors cette fonction ne fait rien (sinon on pourrait ajouter une tâche sans texte)
    if (value.length) {
      onAdd(value); // on envoie la valeur au parent via la props "onAdd"
      setValue(""); // on vide le champ une fois qu'il a été submit
    }
  };

  return (
    <>
      <Text style={styles.label}>{label}</Text>
      <View style={{ ...styles.container, width: width - 40 }}>
        <TextInput
          maxLength={50}
          style={styles.input}
          value={value}
          placeholder="Sortir le chien..."
          onChangeText={(inputValue) => setValue(inputValue)}
        />
        <IconButton onPress={() => handleAddTask(value)}>
          <Feather name="save" size={24} color="#787FF6" />
        </IconButton>
      </View>
    </>
  );
};

export default AddTaskForm;

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
  label: {
    marginBottom: 4,
  },
  input: {
    backgroundColor: "#eee",
    padding: 10,
    borderRadius: 10,
    flex: 1,
    marginRight: 10,
  },
});
